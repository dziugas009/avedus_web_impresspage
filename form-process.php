<?php
$errorMSG = "";

$name = $topic = $email = $message = $lang = $error = "" ;

$errors = array();

if (isset($_POST['lang'])){
	$lang = $_POST['lang'];
}

if (!isset($_POST['name']) || empty($_POST['name'])) {
	if ($lang == 'lt') $error = 'Būtina nurodyti vardą';
	elseif ($lang == 'en') $error = 'Full name required';
    $errors[] = array(
        'input' => 'name',
        'message' => $error
    );

} else {
    $name = test_input($_POST["name"]);
}


if (!isset($_POST['topic']) || empty($_POST['topic'])) {
	if ($lang == 'lt') $error = 'Būtina nurodyti žinutės temą';
	elseif ($lang == 'en') $error = 'Topic required';
    $errors[] = array(
        'input' => 'topic',
        'message' => $error
    );

} else {
    $topic = test_input($_POST["topic"]);
}

if ((!isset($_POST['email']) || empty($_POST['email']))) {
	if ($lang == 'lt') $error = 'Būtina nurodyti teisingą el. pašto adresą';
	elseif ($lang == 'en') $error = 'Email required';
    $errors[] = array(
        'input' => 'email',
        'message' => $error
    );

} else {
    $email = test_input($_POST["email"]);
}

if (!isset($_POST['message']) || empty($_POST['message'])) {
	if ($lang == 'lt') $error = 'Būtina įrašyti žinutę';
	elseif ($lang == 'en') $error = 'Message required';
    $errors[] = array(
        'input' => 'message',
        'message' => $error
    );

} else {
    $message = test_input($_POST["message"]);
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$subject = 'Zinute avedus.lt';

$body = "Nauja žinutė nuo $name:
$message

Siuntėjo kontaktai:
Vardas ir pavardė: $name
El. paštas: $email
Tema: $topic"
;

$headers = "From: $email" . "\r\n";




if (empty($errors)){
	echo '[]';
    $to = (string)$_POST['mainMail'];

    mail($to, $subject, $body, $headers);

}else{
    echo json_encode($errors);
}

?>