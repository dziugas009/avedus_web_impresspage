
<div class="col-md-4 col-md-pull-0">
    <h2>Kontaktiniai duomenys</h2>
    <br/>
    <?php if (isset($success) && $success) { ?>
        <div class="alert alert-success">Sėkmingai išsaugota.</div>
    <?php } ?>

    <div class="well">
        <?= $form->render() ?>
    </div>
</div>
