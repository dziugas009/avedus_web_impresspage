<?php
/**
 * Adds administration grid
 *
 * When this plugin is installed, `Partners` panel appears in administration site.
 *
 */

namespace Plugin\AppControl;


use Ip\Form\Field\Email;
use Ip\Form\Field\Hidden;
use Ip\Form\Field\RepositoryFile;
use Ip\Form\Field\RichText;
use Ip\Form\Field\Submit;
use Ip\Form\Field\Text;
use Ip\Form\Fieldset;
use Ip\Response\Json;

class AdminController
{

    /**
     * @ipSubmenu Kontaktiniai duomenys
     */

    public function index()
    {
        $layout = ipView('view/settings.php');

        $form = $this->settingsForm();

        if (isset($_SESSION['AppControl.saved']) && $_SESSION['AppControl.saved']) {
            $layout->setVariable('success', true);
        }

        $_SESSION['AppControl.saved'] = false;

        $layout->setVariable('form', $form);

        $layout->render();

        return $layout;
    }

    private function settingsForm()
    {
        $form = new \Ip\Form();

        $form->addField(new Hidden([
            'name' => 'aa',
            'value' => 'AppControl.submitSettings'
        ]));

        $langs = ipContent()->getLanguages();

        foreach ($langs as $lang) {
            $fieldset = new Fieldset();

            $fieldset->setLabel($lang->getTitle());

            $lang_code = $lang->getCode();
            $lang_title = strtoupper($lang->getCode());

            $fieldset->addField(new Text([
                'label' => 'Telefono numeris 1' . ' ' . $lang_title,
                'name' => 'phone_number1_' . $lang_code,
                'value' => ipStorage()->get('AppControl', 'phone_number1_' . $lang_code, '+370 5 2045441')
            ]));
            $fieldset->addField(new Text([
                'label' => 'Telefono numeris 2' . ' ' . $lang_title,
                'name' => 'phone_number2_' . $lang_code,
                'value' => ipStorage()->get('AppControl', 'phone_number2_' . $lang_code, '+370 5 2045441')
            ]));

            $fieldset->addField(new Email([
                'label' => 'El.paštas' . ' ' . $lang_title,
                'name' => 'email_' . $lang_code,
                'value' => ipStorage()->get('AppControl', 'email_' . $lang_code, 'info@avedus.lt')
            ]));

            $fieldset->addField(new Text([
                'label' => 'Adresas' . ' ' . $lang_title,
                'name' => 'address_' . $lang_code,
                'value' => ipStorage()->get('AppControl', 'address_' . $lang_code, 'Geležinio Vilko g. 18A, Vilnius')
            ]));

            $fieldset->addField(new Text([
                'label' => 'Linkedin nuoroda' . ' ' . $lang_title,
                'name' => 'linkedin_' . $lang_code,
                'value' => ipStorage()->get('AppControl', 'linkedin_' . $lang_code, 'https://www.linkedin.com/company/avedus')
            ]));

            $fieldset->addField(new Text([
                'label' => 'Google maps adresas' . ' ' . $lang_title,
                'name' => 'google_address_' . $lang_code,
                'value' => ipStorage()->get('AppControl', 'google_address_' . $lang_code, 'https://goo.gl/maps/SK5MWEnXvB42')
            ]));
            $fieldset->addField(new Text([
                'label' => 'Google maps koordinates (platumos)' . ' ' . $lang_title,
                'name' => 'lat_' . $lang_code,
                'value' => ipStorage()->get('AppControl', 'lat_' . $lang_code, '54.699436')
            ]));


            $fieldset->addField(new Text([
                'label' => 'Google maps koordinates (ilgumos)' . ' ' . $lang_title,
                'name' => 'long_' . $lang_code,
                'value' => ipStorage()->get('AppControl', 'long_' . $lang_code, '25.261980')
            ]));
            $form->addFieldset($fieldset);


        }

        $form->addField(new Submit([
            'value' => 'Išsaugoti',
            'attributes' => [
                'style' => 'margin-top:30px;',
                'class' => 'btn btn-success'
            ]
        ]));

        return $form;
    }

    public function submitSettings()
    {
        ipRequest()->mustBePost();
        $post = ipRequest()->getPost();

        $form = $this->settingsForm();
        $errors = $form->validate($post);

        if (!empty($errors)) {
            $data = array(
                'status' => 'error',
                'errors' => $errors
            );

            return new Json($data);
        }

        $_SESSION['AppControl.saved'] = true;

        $skip_fields = [
            'securityToken',
            'antispam',
            'aa',
        ];

        foreach ($post as $field => $value) {
            if (!in_array($field, $skip_fields)) {
                ipStorage()->set('AppControl', $field, $value);
            }
        }

        $data = array(
            'status' => 'ok',
            'redirectUrl' => '/?aa=AppControl.index'
        );

        return new Json($data);
    }

}