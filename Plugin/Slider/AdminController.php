<?php

namespace Plugin\Slider;

class AdminController {
	public function index() {
		$config = [
			'title'  => 'Galerija',
			'table'  => 'slider',
			'sortField' => 'sort_order',
			'createPosition' => 'bottom',
			'allowSort' => true,
			'fields' => [
				[
					'label' => 'Paveiksliukas',
					'field' => 'image',
					'type' => 'RepositoryFile'
				]
			]
		];
		return ipGridController($config);
	}
}