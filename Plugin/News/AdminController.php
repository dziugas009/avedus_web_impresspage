<?php

namespace Plugin\News;

class AdminController {
	public function index() {
		$config = [
			'title'  => 'Naujienos',
			'table'  => 'news',
			'createPosition' => 'bottom',
			'fields' => [
				[
					'label' => 'Ar paslėpti naujieną?',
					'field' => 'is_visible',
					'type' => 'Checkbox'
				],
				[
					'label' => 'Antraštė*',
					'field' => 'header',
					'multilingual' => 1,
				],
				[
					'label' => 'Įžanginis tekstas*',
					'field' => 'card_first_text',
					'type'  => 'Textarea',
					'multilingual' => 1,
					'preview' => false,
				],
				[
					'label' => 'Likęs tekstas*',
					'field' => 'card_rest_text',
					'type'  => 'RichText',
					'multilingual' => 1,
					'preview' => false,
				],
				[
					'label' => 'Paveiksliukas',
					'field' => 'image',
					'type' => 'RepositoryFile'
				]
			]
		];
		return ipGridController($config);
	}
}