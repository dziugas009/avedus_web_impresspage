<?php

namespace Plugin\Clients;

class AdminController {
	public function index() {
		$config = [
			'title'     => 'Atstovaujame',
			'table'     => 'category',
			'sortField' => 'sort_order',
			'allowSort' => true,
			'createPosition' => 'bottom',
			'fields'    => [
				[
					'label'        => 'Kategorijos pavadinimas*',
					'field'        => 'category_title',
					'multilingual' => 1,
					'validators'   => [ 'Required' ]
				],
				[
					'label'  => 'Klientai',
					'type'   => 'Grid',
					'field'  => 'category_id',
					'config' => [
						'title'           => 'Klientai',
						'connectionField' => 'category_id',
						'table'           => 'category_clients',
						'sortField'       => 'sort_order',
						'allowSort'       => true,
						'createPosition' => 'bottom',
						'fields'          => [
							[
								'label'      => 'Kliento pavadinimas*',
								'field'      => 'client_title',
								'validators' => [ 'Required' ]
							],
							[
								'label'      => 'Kliento logotipas*',
								'field'      => 'logo',
								'type'       => 'RepositoryFile',
								'preview'    => false,
								'validators' => [ 'Required' ]
							],
							[
								'label' => 'Nuoroda į kliento puslapį',
								'field' => 'client_url',
							]
						]
					]
				]
			]
		];

		return ipGridController( $config );

	}
}
