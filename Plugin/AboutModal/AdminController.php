<?php

namespace Plugin\AboutModal;

class AdminController {
	public function index() {
		$config = [
			'title'  => 'Apie mus pranešimas',
			'table'  => 'aboutModal',
			'allowCreate' => false,
			'allowSearch' => false,
			'allowDelete' => false,
			'fields' => [
				[
					'label' => 'Pavadinimas',
					'type' => 'Tab'
				],
				[
					'label' => 'Pavadinimas*',
					'field' => 'title',
					'multilingual' => 1,
					'validators' => array('Required')
				],
                [
                    'label' => 'Nuotrauka',
                    'field' => 'img',
                    'type' => 'RepositoryFile',
                    'preview' => false
                ],
				[
					'label' => 'Tekstas po pavadinimu*',
					'field' => 'text',
					'type'  => 'RichText',
					'multilingual' => 1,
					'preview' => false,
					'validators' => array('Required')
				],
				[
					'label' => 'Valdymo politika',
					'type' => 'Tab'
				],
				[
					'label' => 'Valdymo politikos pavadinimas*',
					'field' => 'policy_title',
					'multilingual' => 1,
					'validators' => array('Required')
				],
				[
					'label' => 'Valdymo politikos tekstas*',
					'field' => 'policy_text',
					'type'  => 'RichText',
					'multilingual' => 1,
					'validators' => array('Required')
				],
			]
		];
		return ipGridController($config);
	}
}