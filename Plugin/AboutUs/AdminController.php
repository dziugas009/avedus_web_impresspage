<?php

namespace Plugin\AboutUs;

class AdminController {
	public function index() {
		$config = [
			'title'          => 'Sprendimų kortelės',
			'table'          => 'aboutus',
			'sortField'      => 'sort_order',
			'createPosition' => 'bottom',
			'allowSort'      => true,
			'fields'         => [
				[
					'label'        => 'Pavadinimas*',
					'field'        => 'title',
					'multilingual' => 1,
					'validators'   => array( 'Required' )
				],
				[
					'label'        => 'Tekstas*',
					'field'        => 'text',
					'type'         => 'RichText',
					'multilingual' => 1,
					'preview'      => false,
					'validators'   => array( 'Required' )
				],
				[
					'label' => 'Paveiksliukas',
					'field' => 'image',
					'type'  => 'RepositoryFile'
				]
			]
		];

		return ipGridController( $config );
	}
}