<?php

namespace Plugin\SolutionCards;

class AdminController {
	public function index() {
		$config = [
			'title'  => 'Sprendimų kortelės',
			'table'  => 'solutioncards',
			'sortField' => 'sort_order',
			'allowSort' => true,
			'createPosition' => 'bottom',
            'allowCreate' => false,
            'allowSearch' => false,
            'allowDelete' => false,
			'fields' => [
				[
					'label' => 'Pavadinimas*',
					'field' => 'card_title',
					'multilingual' => 1,
					'validators' => array('Required')
				],
				[
					'label' => 'Įžanginis tekstas*',
					'field' => 'card_first_text',
					'type'  => 'RichText',
					'multilingual' => 1,
					'preview' => false,
					'validators' => array('Required')
				],
				[
					'label' => 'Likęs tekstas*',
					'field' => 'card_rest_text',
					'type'  => 'RichText',
					'multilingual' => 1,
					'preview' => false,
					'validators' => array('Required')
				],
				[
					'label' => 'Išorinis Paveiksliukas',
					'field' => 'out_image',
					'type' => 'RepositoryFile'
				],
                [
                    'label' => 'Vidinis paveiksliukas',
                    'field' => 'in_image',
                    'type' => 'RepositoryFile'
                ]
			]
		];
		return ipGridController($config);
	}
}