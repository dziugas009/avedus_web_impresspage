<?= ipView('_header.php')->render() ?>
<?php
$languageCode = ipContent()->getCurrentLanguage()->getCode();
$solutionCards = ipDb()->fetchAll('SELECT * FROM ip_solutioncards sc LEFT JOIN ip_solutioncards_language scl ON sc.id = scl.itemId
WHERE scl.language = "' . $languageCode . '"
ORDER BY sc.sort_order ASC
');

$aboutUs = ipDb()->fetchAll('SELECT * FROM ip_aboutus au LEFT JOIN ip_aboutus_language aul ON au.id = aul.itemId
WHERE aul.language = "' . $languageCode . '"
ORDER BY au.sort_order ASC
')
?>

    <div class="safety-wrapper">

        <?= ipView('navbar.php')->render() ?>

        <section class="page-section section-framed inner-layout-wrapper" id="home">
            <div class="container-fluid">
                <div class="row hero-wrapper">
                    <div class="col-sm-12 col-md-6 hero-text-wrapper">
                        <?= ipSlot('text', array(
                            'id' => 'header1',
                            'tag' => 'h1',
                            'class' => 'header-1 text-dark',
                            'default' => 'Intelektualūs IT sprendimai'
                        )) ?>
                        <?= ipSlot('text', array(
                            'id' => 'intro-after-header1',
                            'tag' => 'p',
                            'class' => 'tb tb-intro',
                            'default' => 'AVEDUS yra viena iš informacinių tinklų saugumo lyderių Lietuvoje. Išsiskiriame savo diskretiškumu, darbo kultūra ir kompetencija.'
                        )) ?>

                        <ul class="list-inline mt-60">
                            <li>
                                <a href="#" data-toggle="modal" data-target="#aboutModal"
                                   class="btn btn-main formOpener">
                                    <?= __('SUŽINOTI DAUGIAU', 'Avedus'); ?></a>
                            </li>
                            <li>
                                <a href="#solutions"
                                   class="btn btn-default"> <?= __('SIŪLOMI SPRENDIMAI', 'Avedus'); ?></a>
                            </li>
                        </ul>

                    </div>
                    <div class="col-sm-6 hero-animation">
                        <div id="hand1">
                            <div id="hand1-wrapper">
                                <img class="img-responsive" src="<?= ipThemeUrl('assets/img/hand1-lower-part.png') ?>"
                                     alt="Ranka" title="Avedus" id="hand1-part2">
                                <img src="<?= ipThemeUrl('assets/img/cord.png') ?>" alt="Laidas" id="cord">
                                <img class="img-responsive" src="<?= ipThemeUrl('assets/img/hand1-upper-part.png') ?>"
                                     alt="Ranka" title="Avedus" id="hand1-part1">
                            </div>
                        </div>
                        <div id="hand2">
                            <img class="img-responsive" title="Avedus" src="<?= ipThemeUrl('assets/img/hand2.png') ?>" alt="Ranka">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?= ipView('solution.php')->render() ?>

        <?= ipView('aboutUs.php')->render() ?>


        <section class="page-section section-framed" id="clients">
            <div class="container-fluid main-wrapper">
                <div class="row">
                    <div class="col-sm-12">
                        <?= ipSlot('text', array(
                            'id' => 'clients-header1',
                            'tag' => 'h2',
                            'class' => 'header-2 text-center',
                            'default' => 'Atstovaujame'
                        )) ?>
                    </div>
                </div>

                <div class="row">
                    <?= ipView('clients.php')->render() ?>
                </div>
            </div>
        </section>
        <!-- Slider -->
        <?= ipView('slider.php')->render() ?>

        <!-- Naujienos -->
        <?= ipView('news.php')->render() ?>

        <!-- Forma -->
        <section class="page-section section-framed" id="contacts">
            <div class="container-fluid main-wrapper">
                <div class="row">
                    <div class="col-sm-12">
                        <?= ipSlot('text', [
                            'id' => 'contactUsHeader1',
                            'tag' => 'h2',
                            'class' => 'header-2 text-center',
                            'default' => 'Susisiekite'
                        ]);
                        ?>
                    </div>
                </div>
                <div id="mainForm" class="form-wrapper">
                    <?= ipView('form.php')->render() ?>
                </div>
            </div>
        </section>


        <input type="hidden" name="lat" value="<?= ipStorage()->get('AppControl', 'lat_' . $languageCode) ?>">
        <input type="hidden" name="long" value="<?= ipStorage()->get('AppControl', 'long_' . $languageCode) ?>">

        <section class="page-section" id="contacts-map">

            <div class="map-wrapper">
                <div id="map"></div>
            </div>

            <div class="main-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h2 class="header-2">
                                <?= __('Kontaktai', 'Avedus'); ?>
                            </h2>
                        </div>
                    </div>
                    <div class="row text-center mt-40 row-inlined row-inlined-middle">
                        <div class="col-sm-3 col-inline">
                            <div class="tb tb-iconned tb-address">
                                <a title="Google address" href="<?= ipStorage()->get('AppControl', 'google_address_' . $languageCode) ?>" target="_blank">
                                    <?= ipStorage()->get('AppControl', 'address_' . $languageCode) ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-inline">
                            <div class="tb tb-iconned tb-phone">
                                <a title="First Phone" href="tel:<?= str_replace(' ', '', ipStorage()->get('AppControl', 'phone_number1_' . $languageCode)) ?>"
                                   class="js-tel js-mask-tel no-break">
                                    <?= ipStorage()->get('AppControl', 'phone_number1_' . $languageCode) ?>
                                </a>,
                                <a title="Second Phone" href="tel:<?= str_replace(' ', '', ipStorage()->get('AppControl', 'phone_number2_' . $languageCode)) ?>" class="js-tel js-mask-tel no-break">
                                    <?= ipStorage()->get('AppControl', 'phone_number2_' . $languageCode) ?>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-inline">
                            <div class="tb tb-iconned tb-email">
                                <a title="Email" href="mailto:<?= ipStorage()->get('AppControl', 'email_' . $languageCode) ?>"
                                   target="_blank">
                                    <?= ipStorage()->get('AppControl', 'email_' . $languageCode) ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <a title="Navigation" class="btn btn-white" id="toNavigation" href="https://goo.gl/xw6NE7" target="_blank">
                <?= __('Įjungti navigaciją', 'Avedus'); ?>
            </a>

        </section>

        <section class="page-section bg-gray" id="requisites">
            <div class="main-wrapper">
                <div class="container-fluid">
<!--                    <div class="row">-->
<!--                        <div class="col-sm-12 text-center">-->
<!--                            --><?//= ipSlot('text', array(
//                                'id' => 'requisites_header',
//                                'tag' => 'h2',
//                                'class' => 'header-2',
//                                'default' => 'Rekvizitai'
//                            )) ?>
<!--                        </div>-->
<!--                    </div>-->

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <div class="tb pseudo-list">
                                <?= ipSlot('text', array(
                                    'id' => 'requisites_li1',
                                    'tag' => 'p',
                                    'container' => false,
                                    'default' => 'UAB "AVEDUS"'
                                ))?>
                                <?= ipSlot('text', array(
                                    'id' => 'requisites_li2',
                                    'tag' => 'p',
                                    'container' => false,
                                    'default' => 'Įmonės kodas: 300583901'
                                ))?>
                                <?= ipSlot('text', array(
                                    'id' => 'requisites_li3',
                                    'tag' => 'p',
                                    'container' => false,
                                    'default' => 'PVM mokėtojo kodas: LT100002530119'
                                ))?>

                                <?= ipSlot('text', array(
                                    'id' => 'requisites_li4',
                                    'tag' => 'p',
                                    'container' => false,
                                    'default' => 'A/s: LT03 7300 0100 9676 7590'
                                ))?>
                                <?= ipSlot('text', array(
                                    'id' => 'requisites_li5',
                                    'tag' => 'p',
                                    'container' => false,
                                    'default' => 'AB bankas ”Swedbank”, b.kodas 73000'
                                ))?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer class="main-footer text-dark">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <nav class="footer-nav text-center">
                            <ul class="list-inline">
                                <li>
                                    <a title="Solutions" href="#solutions">
                                        <?= __('Sprendimai', 'Avedus'); ?>
                                    </a>
                                </li>
                                <li>
                                    <a title="About us" href="#about">
                                        <?= __('Apie mus', 'Avedus'); ?>
                                    </a>
                                </li>
                                <li>
                                    <a title="Represent" href="#clients">
                                        <?= __('Atstovaujame', 'Avedus'); ?>
                                    </a>
                                </li>
                                <li>
                                    <a title="Gallery" href="#main-slider">
                                        <?= __('Galerija', 'Avedus'); ?>
                                    </a>
                                </li>
                                <li>
                                    <a title="News" href="#news">
                                        <?= __('Naujienos', 'Avedus'); ?>
                                    </a>
                                </li>
                                <li>
                                    <a title="Contact us" href="#contacts">
                                        <?= __('Susisiekti', 'Avedus'); ?>
                                    </a>
                                </li>
                                <li>
                                    <a title="Contacts" href="#contacts-map">
                                        <?= __('Kontaktai', 'Avedus'); ?>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

<!--                <div class="row text-center row-copyright">-->
<!--                    <div class="col-sm-12">-->
<!--                        <div class="signature">-->
<!--                            --><?//= __('© 2012–2017 UAB ”Avedus” – IT Sprendimai. Visos teisės saugomos.', 'Avedus'); ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="row text-center">
                    <div class="col-sm-12">
                        <a title="LinkedIn" href="<?= ipStorage()->get('AppControl', 'linkedin_' . $languageCode) ?>"
                           class="btn btn-square-icon" target="_blank">
                            <i class="fa fa-linkedin" aria-label="linkedIn"></i>
                        </a>
                    </div>
                </div>

            </div>
        </footer>

    </div>


    <!-- NAVIGATION modal -->
    <div class="modal fade pagelike-modal" id="navigationModal" tabindex="-1" role="dialog"
         aria-labelledby="navigationModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <?= ipView('navigation_modal.php')->render() ?>
                </div>
            </div>
        </div>
    </div>


    <!-- ABOUT modal -->
<?= ipView('about_modal.php')->render() ?>


    <!-- SOLUTIONS modal -->
<?= ipView('solution_modal.php')->render() ?>


    <!-- NEWS modal -->
<?= ipView('news_modal.php')->render() ?>

<?= ipView('_footer.php')->render() ?>