<?php echo ipDoctypeDeclaration(); ?>
<html<?php echo ipHtmlAttributes(); ?>>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!-- All CSS libs -->
	<?php ipAddCss('assets/css/libs.min.css') ?>
	<!-- Custom CSS -->
	<?php ipAddCss('assets/css/style.min.css') ?>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <script>
        var formProcess = "http://avedus.imas.lt/form-process.php";
        var url = "http://<?php echo $_SERVER['SERVER_NAME']; ?>"
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-107918928-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-107918928-1');
    </script>
	<?php echo ipHead()?>
</head>

<body data-spy="scroll" data-target=".navbar-scrollspy" data-offset="50">