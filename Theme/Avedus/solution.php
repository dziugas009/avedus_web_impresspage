<?php
$languageCode  = ipContent()->getCurrentLanguage()->getCode();
$solutionCards = ipDb()->fetchAll( 'SELECT * FROM ip_solutioncards sc LEFT JOIN ip_solutioncards_language scl ON sc.id = scl.itemId
WHERE scl.language = "' . $languageCode . '"
ORDER BY sc.sort_order ASC
' );
?>
<section class="page-section service-section section-framed" id="solutions">
	<div class="container-fluid">
		<div class="row card-image-background-wrapper">
                <?php foreach ( $solutionCards as $solutionCard ) { ?>
                    <?php
                    $options   = array(
                        'type'    => 'center',
                        'width'   => 416,
                        'height'  => 435,
                        'quality' => 100,
                    );
                    $thumbnail = ipReflection( $solutionCard['out_image'], $options );
                    ?>
                    <div class="col-sm-12 col-md-4 card card-image-background" style="background-image: url(<?= ipFileUrl( $thumbnail ); ?>)">
                        <div class="card-shade"></div>
                        <div class="card-background">
                        </div>

                        <div class="card-content-wrapper">
                            <h2 class="card-title">
                                <?= $solutionCard['card_title'] ?>
                            </h2>
                            <div class="card-tb">
                                <?= $solutionCard['card_first_text'] ?>
                            </div>
                            <a href="#" data-toggle="modal" data-target="#solutionModal<?= $solutionCard['id'] ?>"
                               class="btn btn-white formOpener"><?= __('PLAČIAU', 'Avedus') ?></a>
                        </div>

                    </div>
                <?php } ?>
        </div>
	</div>
</section>