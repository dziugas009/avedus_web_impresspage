<nav class="main-navigation navbar navbar-scrollspy">
	<ul class="nav navbar list-unstyled">
		<li>
			<a title="Solutions" href="#solutions">
				<?= __('Sprendimai', 'Avedus'); ?>
			</a>
		</li>
		<li>
			<a title="About Us" href="#about">
				<?= __('Apie mus', 'Avedus'); ?>
			</a>
		</li>
		<li>
			<a title="Represent" href="#clients">
				<?= __('Atstovaujame', 'Avedus'); ?>
			</a>
		</li>
		<li>
			<a title="Gallery" href="#main-slider">
				<?= __('Galerija', 'Avedus'); ?>
			</a>
		</li>
		<li>
			<a title="News" href="#news">
				<?= __('Naujienos', 'Avedus'); ?>
			</a>
		</li>
		<li>
			<a title="Contact Us" href="#contacts">
				<?= __('Susisiekti', 'Avedus'); ?>
			</a>
		</li>
		<li>
			<a title="Contacts" href="#contacts-map">
				<?= __('Kontaktai', 'Avedus'); ?>
			</a>
		</li>
	</ul>
</nav>