<?php
$languageCode  = ipContent()->getCurrentLanguage()->getCode();
$solutionCards = ipDb()->fetchAll( 'SELECT * FROM ip_solutioncards sc LEFT JOIN ip_solutioncards_language scl ON sc.id = scl.itemId
WHERE scl.language = "' . $languageCode . '"
ORDER BY sc.sort_order ASC
' );
?>
<?php foreach ( $solutionCards as $solutionCard ) { ?>

<div class="modal fade pagelike-modal solution-modal" id="solutionModal<?= $solutionCard['id'] ?>" tabindex="-1" role="dialog"
     aria-labelledby="solutionModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content text-dark">
            <?php
            $options   = array(
                'type'    => 'center',
                'width'   => 970,
                'height'  => 260,
                'quality' => 100,
            );
            $thumbnail = ipReflection( $solutionCard['in_image'], $options );
            ?>
			<div class="modal-content-wrapper">
                <div class="modal-header">
                    <div class="header-imaged" style="background-image: url(<?= ipFileUrl($thumbnail)?>)">
                        <div class="header-overlay"></div>
                        <div class="header-content">
                            <h2 class="header header-2 text-center">
                                <?= $solutionCard['card_title'] ?>
                            </h2>
                        </div>
                    </div>
                </div>

				<div class="modal-body">
					<section>
						<div class="container-fluid">
							<div class="row">
								<div class="col-sm-12 tb tb-spacedout text-lighter">
									<?= $solutionCard['card_first_text'] ?>
									<?= $solutionCard['card_rest_text'] ?>
								</div>
							</div>

							<div class="row text-center mt-40">
								<div class="col-sm-12">
									<?= ipSlot('text', [
										'id' => 'contactUsHeader1',
										'tag' => 'h2',
										'class' => 'header-2',
										'default' => 'Susisiekite'
									]);
									?>
								</div>
							</div>

							<div class="row mt-40">
								<div id="formSolution" class="col-sm-12">
									<?= ipView('form.php')->render() ?>
								</div>
							</div>
						</div>
					</section>
				</div>

			</div>
		</div>
	</div>
    <div class="modal-footer text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">
                                         <i class="fa fa-times"></i>
                                        <p><?= __('Grįžti atgal', 'Avedus') ?></p>
                                    </span>
        </button>
    </div>
</div>
<?php } ?>