<?php $languageCode = ipContent()->getCurrentLanguage()->getCode();
?>
<nav class="navbar navbar-default navbar-custom" data-spy="affix" data-offset-top="20">
    <div class="container-fluid">
        <div class="navbar-header">

            <a class="navbar-brand" href="http://<?= $_SERVER['SERVER_NAME'] ?>">
                <img class="brand-img" src="<?= ipThemeUrl( 'assets/img/avedus_logo.png' ) ?>" alt="Avedus" title="Avedus">
            </a>
            <div class="transform-group">
                <button type="button" class="navbar-toggle collapsed" data-toggle="modal" data-backdrop="false"
                        data-target="#navigationModal">
                    <div class="valign">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>
                    <div class="valign">
                        <p>Meniu</p>
                    </div>
                </button>
                <div class="valign">
                    <ul class="list-slashed" id="lang-select">
						<?php $langs = ipContent()->getLanguages();
						foreach ( $langs as $lang ) {
							$lang_code = $lang->getCode()
							?>
                            <li>
                                <a href="http://<?= $_SERVER['SERVER_NAME'] ?>/<?= $lang_code ?>"><?= strtoupper($lang_code)?></a>
                            </li>

						<?php } ?>
                    </ul>
                </div>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?= ipStorage()->get( 'AppControl', 'linkedin_' . $languageCode)?>" target="_blank" title="LinkedIn" class="tb tb-iconned tb-linkedin"></a>
                </li>
                <li class="tb tb-iconned tb-phone">
                    <a href="tel:<?= str_replace( ' ', '', ipStorage()->get( 'AppControl', 'phone_number1_' . $languageCode ) ) ?>"
                       title="First Phone" class="js-tel js-mask-tel">
						<?= ipStorage()->get( 'AppControl', 'phone_number1_' . $languageCode ) ?>
                    </a>,
                    <a href="tel:<?= str_replace( ' ', '', ipStorage()->get( 'AppControl', 'phone_number2_' . $languageCode ) ) ?>"
                       title="Second Phone" class="js-tel js-mask-tel">
                        <?= ipStorage()->get( 'AppControl', 'phone_number2_' . $languageCode ) ?>
                    </a>
                </li>
                <li><a class="btn btn-main" href="#contacts"><?= __( 'Susisiekti', 'Avedus' ); ?></a></li>
            </ul>

            <div class="modal-close-wrapper">
                <a class="btn formOpener" href="#" id="modal-close">x</a>
            </div>
        </div>

    </div>
</nav>