$(document).ready(function () {

    var myLatLng = {lat: parseFloat($('input[name="lat"]').val()), lng: parseFloat($('input[name="long"]').val())};
    var newsModal = $('#newsModal');


    $('#navigationModal').on('shown.bs.modal', function () {
        $(".main-navigation").find("li").each(function (i) {
            $(this).delay(100 * i).queue(function (nxt) {
                $(this).addClass('loaded');
                nxt();
            });
        });
    });

    $('#navigationModal').on('hidden.bs.modal', function () {
        $(".main-navigation").find("li").each(function (i) {
            $(this).removeClass('loaded');
        });
    });


    /* * * * * * * * * * * * * * * * * * * * *
        SCROLL TO SECTION
     * * * * * * * * * * * * * * * * * * * * */


    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(this).blur();
        var section = $(this).attr('href');

        if (section != '#' && $(section).length > 0 && $(section).hasClass('page-section')) {
            $('#navigationModal').modal('hide');

            scrollToPart(section);
        }
    });

    function scrollToPart(section) {
        var navHeight;

        if ($('.ipAdminPanel').length) {
            navHeight = $('.navbar').outerHeight() + $('.ipAdminPanel').outerHeight();
        } else {
            navHeight = $('.navbar').outerHeight();
        }

        var scrollPos;
        if (section != '#' && $('body').find(section).length > 0) {
            scrollPos = $(section).offset().top - navHeight;

            $('html, body').stop(true, true).animate({
                scrollTop: scrollPos
            }, 1000);
        } else {
            return;
        }
    }


    /* * * * * * * * * * * * * * * * * * * * *
         HERO ANIMATION
     * * * * * * * * * * * * * * * * * * * * */

    var heroAnimation = $('.hero-animation');

    if (isMobile()) {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 50 && heroAnimation.hasClass('animated')) {
                heroAnimation.removeClass('animated');
                moveHand1(false);
                moveHand2(false);

                setTimeout(function () {
                    moveCord(false);
                }, 500);


            } else if ($(window).scrollTop() < 50 && !heroAnimation.hasClass('animated')) {
                heroAnimation.addClass('animated');

                setTimeout(function () {
                    moveCord(true);
                }, 100);
                moveHand1(true);
                moveHand2(true);
            }

        });
    } else {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 150 && heroAnimation.hasClass('animated')) {
                heroAnimation.removeClass('animated');
                moveHand1(false);
                moveHand2(false);

                setTimeout(function () {
                    moveCord(false);
                }, 500);

            } else if ($(window).scrollTop() < 150 && !heroAnimation.hasClass('animated')) {
                heroAnimation.addClass('animated');

                setTimeout(function () {
                    moveCord(true);
                }, 100);
                moveHand1(true);
                moveHand2(true);
            }

        });
    }

    var hand1 = $('#hand1');
    var hand2 = $('#hand2');
    var cord = $('#cord');

    function moveHand1(isMoveUp) {
        if (isMoveUp) {
            if (isMobile()) {
                hand1.animate({
                    top: '-15px',
                    left: '55%'
                }, 1400, "swing");
            } else {
                hand1.animate({
                    top: '-20px',
                    left: '55%'
                }, 1400, "swing");
            }

        } else {
            if (isMobile()) {
                hand1.animate({
                    top: '-300px',
                    left: '55%'
                }, 1400, "swing");
            } else {
                hand1.animate({
                    top: '-600px',
                    left: '55%'
                }, 800, "linear");
            }
        }
    }

    function moveHand2(isMoveUp) {
        if (isMoveUp) {
            if (isMobile()) {
                hand2.animate({
                    top: '220px',
                    right: '0'
                }, 800, "swing");
            } else {
                hand2.animate({
                    bottom: '-60px',
                    right: '20px'
                }, 800, "swing");
            }
        } else {
            if (isMobile()) {
                hand2.animate({
                    top: '220px',
                    right: '-220px'
                }, 600, "linear");
            } else {
                hand2.animate({
                    bottom: '-320px',
                    right: '0px'
                }, 600, "linear");
            }
        }
    }


    function moveCord(isMoveUp) {
        if (isMoveUp) {
            if (isMobile()) {

            } else {
                cord.animate({
                    top: '0',
                    left: '-138px'
                }, 1800, "swing");
            }
        } else {
            if (isMobile()) {

            } else {
                cord.animate({
                    top: '-800px',
                    left: '-500px'
                }, 600, "linear");
            }
        }
    }

    $('body').ready(function () {
        setTimeout(function () {
            moveHand1(true);
            moveHand2(true);
            setTimeout(function () {
                moveCord(true);
            }, 500);
            heroAnimation.addClass('animated');
        }, 1000)
    });

    var body = $(document.body);


    /* * * * * * * * * * * * * * * * * * * * *
     MAIN NAVBAR AFFIX
     * * * * * * * * * * * * * * * * * * * * */

    $(".navbar-custom").on('affix.bs.affix', function () {
        $(this).removeClass('navbar-transparent');
    });

    $('.navbar-custom').on('affixed-top.bs.affix', function () {
        if ($(this).hasClass('affixing-transparent')) {
            $(this).addClass('navbar-transparent');
        }
    });


    /* * * * * * * * * * * * * * * * * * * * *
         MASKING
     * * * * * * * * * * * * * * * * * * * * */

    $('.js-mask-date').mask('0000 / 00 / 00');
    $('.js-mask-tel').mask('+000 0 0000000');


    /* * * * * * * * * * * * * * * * * * * * *
        SWIPERS
     * * * * * * * * * * * * * * * * * * * * */


    // MAIN SWIPER

    // var gallerySwipper = new Swiper('#swiper-main', {
    //     // Optional parameters
    //     direction: 'horizontal',
    //     loop: true,
    //     nextButton: '.main-swiper-button-next',
    //     prevButton: '.main-swiper-button-prev',
    //
    //     autoplay: 8000,
    //     autoplayDisableOnInteraction: false,
    //     speed: 800
    // });


    // NEWS SWIPER
    if ($('#newsCounter').val() < 4) var loopTF = false;
    else var loopTF = true;

    function controlSwiper(swiper, slidesNoForActive) {
        if (swiper.slides.length > slidesNoForActive) {
            swiper.unlockSwipes();
            swiper.startAutoplay();
            swiper.params.loop = true;
            $(swiper.container).removeClass('noSwiping');

        } else {
            swiper.lockSwipes();
            swiper.params.loop = false;
            $(swiper.container).addClass('noSwiping');
        }

        swiper.update(true)
    }



    var newsSwipper = new Swiper('#swiper-news', {
        direction: 'horizontal',
        loop: loopTF,
        autoplay: 5000,
        autoplayDisableOnInteraction: false,
        speed: 800,
        slidesPerGroup: 3,
        slidesPerView: 3,
        spaceBetween: 15,
        pagination: '.swiper-news-pagination-modal',
        nextButton: '.swiper-news-arrow.swiper-button-next',
        prevButton: '.swiper-news-arrow.swiper-button-prev',
        breakpoints: {
            460: {
                slidesPerView: 1,
                spaceBetween: 10,
                slidesPerGroup: 1,
            },
            991: {
                slidesPerView: 2,
                spaceBetween: 20,
                slidesPerGroup: 1,
            },
        },
        onInit: function(swiper) {
            $(swiper.container).find('.truncate').each(function () {
                $(this).dotdotdot({
                    ellipsis: "...",
                    truncate: "word",
                    keep: null,
                    watch: "window",
                });
            });
        },
    });

    var newsSwipperModal = new Swiper('#swiper-news-modal', {
        direction: 'horizontal',
        // loop: loopTF,
        autoplay: 5000,
        autoplayDisableOnInteraction: false,
        speed: 800,
        slidesPerGroup: 3,
        slidesPerView: 3,
        spaceBetween: 15,
        pagination: '.swiper-news-pagination-modal',
        nextButton: '.swiper-news-arrow-modal.swiper-button-next',
        prevButton: '.swiper-news-arrow-modal.swiper-button-prev',
        breakpoints: {
            460: {
                slidesPerView: 1,
                spaceBetween: 10,
                slidesPerGroup: 1,
            },
            991: {
                slidesPerView: 2,
                spaceBetween: 20,
                slidesPerGroup: 1,
            },
        }
    });

    // $(window).on('resize', function () {
    //     newsSwipperModal.update(true);
    //     newsSwipper.update(true);
    // });

    // if (!loopTF && !isMobile()){
    //     // newsSwipper.lockSwipes();
    //     // newsSwipperModal.lockSwipes();
    //     $('#swiper-news').addClass('full-width');
    //     $('#swiper-news-modal').addClass('full-width');
    // } else {
    //     // newsSwipper.unlockSwipes();
    //     // newsSwipperModal.unlockSwipes();
    //     $('#swiper-news').removeClass('full-width');
    //     $('#swiper-news-modal').removeClass('full-width');
    // }

    newsModal.on('shown.bs.modal', function () {
        newsSwipperModal.update(true);

        $('#swiper-news-modal').find('.truncate').each(function () {
            $(this).dotdotdot({
                ellipsis: "...",
                truncate: "word",
                keep: null,
                watch: "window",
            });
        });

    });



    /* * * * * * * * * * * * * * * * * * * * *
         MODAL CONTROL
     * * * * * * * * * * * * * * * * * * * * */


    $('.modal').on('hide.bs.modal', function () {
        var thisModal = $(this);
        body.removeClass('modalIsOpen');
        // newsSwipper.update(true);
        thisModal.find('.has-error').each(function(){
            $(this).removeClass('has-error')
        });
    });


    $('body').on('hidden.bs.modal', '.modal', function () {
        $('.btn').each(function () {
            $(this).blur();
        });
        $('button').each(function () {
            $(this).blur();
        });
    });

    $('.modal').on('show.bs.modal', function () {
        body.addClass('modalIsOpen');
    });


    newsModal.find('.card-news').on('click', function (e) {
        e.preventDefault();
        var selector = $(this);
        resetNewsModal();
        addNewsModalContent(selector);
    });


    resetNewsModal = function () {
        newsModal.find('.modal-data').html('');
        newsModal.find('.modal-date').find('.js-mask-date').html('');
        newsModal.find('.modal-image').attr('src', '');
    };

    addNewsModalContent = function (selector) {

        var modalTitle = selector.find('.card-title').text();
        var modalDate = selector.find('.js-mask-date').html();

        if (isMobile()) {
            var modalImage = selector.attr('data-news-image-mobile');
        } else {
            var modalImage = selector.attr('data-news-image');
        }

        var modalText = selector.attr('data-news-text');
        var newsModalInnerContent = newsModal.find('.inner-wrapper');

        newsModalInnerContent.find('.modal-title').text(modalTitle);
        newsModalInnerContent.find('.js-mask-date').html(modalDate);
        newsModalInnerContent.find('.modal-image').attr('src', modalImage);
        newsModalInnerContent.find('.modal-body-text').html(modalText);

        newsModal.animate({scrollTop: 0}, 1000);
    };


    /* * * * * * * * * * * * * * * * * * * * *
         MAP
     * * * * * * * * * * * * * * * * * * * * */

    function initMap() {

        var styles = [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#1d242e"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#374257"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#1d242e"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        // "color": "#374257"
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        // "color": "#9e9e9e"
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        // "color": "#bdbdbd"
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#374257"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#1d242e"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#1d242e"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#222935"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#8a8a8a"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#222935"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#222935"
                    }
                ]
            },
            {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#4e4e4e"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#374257"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#101219"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#374257"
                    }
                ]
            }
        ];


        var options = {
            mapTypeControlOptions: {
                mapTypeIds: ['Styled']
            },
            center: new google.maps.LatLng(myLatLng),
            zoom: 14,
            scrollwheel: true,
            disableDefaultUI: true,
            mapTypeId: 'Styled',
            draggable: true,
        };
        var div = document.getElementById('map');
        var map = new google.maps.Map(div, options);
        var styledMapType = new google.maps.StyledMapType(styles, {name: 'Styled'});
        map.mapTypes.set('Styled', styledMapType);

        var icon = {
            url: url + '/Theme/Avedus/assets/img/map-marker.png',
            size: new google.maps.Size(48, 60),
            scaledSize: new google.maps.Size(24, 30),
            origin: new google.maps.Point(-14, -30)
        };

        var marker = new google.maps.Marker({
            map: map,
            position: myLatLng,
            title: 'Avedus',
            icon: icon,
            optimized: false,
        });
    };

    initMap();


    $('#modal-close').on('click', function () {
        $('.modal.in').each(function () {
            $(this).modal('hide');
        })
    });
    $('.specialClass').on('click', function () {
        console.log($(this));
        var newsDate = $(this).find('.dateNews').val();
        $('#newsModalTitle').html($(this).find('#titleNews').val());
        $('#modalDateHeader').find('#newsModalReleaseDate').html(newsDate);
        if (isMobile()) {
            $('#newsModalImg').attr('src', $(this).find('#imgNewsMob').val());
        } else {
            $('#newsModalImg').attr('src', $(this).find('#imgNews').val());
        }        $('#newsModalText').html($(this).find('#firstNews').val() + $(this).find('#restNews').val());
    });


    //ajax submit

    var contactsForm = $('#mainForm').find("form");


    contactsForm.on("submit", function (e) {
        e.preventDefault();
        submitForm();
    });

    function submitForm() {
        var formData = contactsForm.serialize();
        $.ajax({
            type: "POST",
            url: formProcess,
            data: formData,
            dataType: 'json',
            success: function (text) {
                contactsForm.find('.form-group').find('.error-message').remove();
                contactsForm.find('.has-error').removeClass('has-error');
                contactsForm.find('.error-message').remove();
                contactsForm.find('#alert-success').text('');
                if (text == '') {
                    formSuccess();
                } else {
                    $.each(text, function (inx, value) {
                        contactsForm.find('[name="' + value.input + '"]').after('<p class="error-message">' + value.message + '</p>');
                        contactsForm.find('[name="' + value.input + '"]').parents('.form-group-input').addClass('has-error');
                    });
                }
            },
            error: function (text) {
                console.log(text)
            }
        });
    }

    function formSuccess() {
        contactsForm[0].reset();

        $('#alert-success').text('Ačiū. Jūsų žinutė išsiųsta sėkmingai.');

    }


    var contactsForm2 = $('#formAbout').find("form");


    contactsForm2.on("submit", function (e) {
        e.preventDefault();
        submitForm2();
    });

    function submitForm2() {
        var formData2 = contactsForm2.serialize();
        $.ajax({
            type: "POST",
            url: formProcess,
            data: formData2,
            dataType: 'json',
            success: function (text) {
                contactsForm2.find('.form-group').find('.error-message').remove();
                contactsForm2.find('.has-error').removeClass('has-error');
                contactsForm2.find('#alert-success').text('');
                if (text == '') {
                    formSuccess2();
                } else {
                    $.each(text, function (inx, value) {
                        contactsForm2.find('[name="' + value.input + '"]').after('<p class="error-message">' + value.message + '</p>');
                        contactsForm2.find('[name="' + value.input + '"]').parents('.form-group-input').addClass('has-error');
                    });
                }
            },
            error: function (text) {
                console.log('fail')
            }
        });
    }

    function formSuccess2() {
        contactsForm2[0].reset();

        contactsForm2.find('#alert-success').text('Ačiū. Jūsų žinutė išsiųsta sėkmingai.');

    }


    $('.solution-modal').on('show.bs.modal', function(){
        var thisSolutionModal = $(this);

        var contactsForm3 = thisSolutionModal.find('#formSolution').find("form");

        $('.formOpener').click(function () {
            $('.form-group-input').find('.error-message').remove();
            $('.form-group-input').removeClass('has-error');
        });

        contactsForm3.on("submit", function (e) {
            e.preventDefault();
            submitForm3();
        });

        function submitForm3() {
            var formData3 = contactsForm3.serialize();
            $.ajax({
                type: "POST",
                url: formProcess,
                data: formData3,
                dataType: 'json',
                success: function (text) {
                    contactsForm3.find('.form-group').find('.error-message').remove();
                    contactsForm3.find('.has-error').removeClass('has-error');
                    contactsForm3.find('#alert-success').text('');
                    if (text == '') {
                        contactsForm3.find('#formSolution').find("button").html('')
                        formSuccess3();
                    } else {
                        $.each(text, function (inx, value) {
                            contactsForm3.find('[name="' + value.input + '"]').after('<p class="error-message">' + value.message + '</p>');
                            contactsForm3.find('[name="' + value.input + '"]').parents('.form-group-input').addClass('has-error');
                        });
                    }
                }
            });
        }

        function formSuccess3() {
            contactsForm3[0].reset();
            contactsForm3.find('#alert-success').text('Ačiū. Jūsų žinutė išsiųsta sėkmingai.');
        }

    });


    $('#moreGallery').on('click', function () {
        if ($(this).text() == 'SHOW MORE' || $(this).text() == 'SHOW LESS') {

            $(this).html(function (i, text) {
                $(this).html($(this).text() === "SHOW MORE" ? "SHOW LESS" : "SHOW MORE");
            });
        }
        if ($(this).text() == 'RODYTI DAUGIAU' || $(this).text() == 'RODYTI MAŽIAU') {


            $(this).html(function (i, text) {
                $(this).html($(this).text() === "RODYTI DAUGIAU" ? "RODYTI MAŽIAU" : "RODYTI DAUGIAU");
            });
        }

        $('.moreImg').fadeToggle();
    });

    $('.gallery-img-link').each(function(){
        var thisImgLink = $(this);

        if (isMobile) {
            thisImgLink.attr('href', thisImgLink.attr('data-mobile-img-url'))
        } else {
            thisImgLink.attr('href', thisImgLink.attr('data-mobile-img-url'))
        }
    })


    if ($('#moreGallery').text() == 'RODYTI DAUGIAU') {
        lightbox.option({
            'albumLabel': '%1 iš %2',
            'wrapAround': true,
            'positionFromTop': 90,
            'alwaysShowNavOnTouchDevices': true,
            'disableScrolling': true,
            'fadeDuration': 200,
            'resizeDuration': 300
        });
    }else{
        lightbox.option({
            'albumLabel': '%1 of %2',
            'wrapAround': true,
            'positionFromTop': 90,
            'alwaysShowNavOnTouchDevices': true,
            'disableScrolling': true,
            'fadeDuration': 200,
            'resizeDuration': 300
        });
    }




    /* * * * * * * * * * * * * * * * * * * * *
       STYLES
     * * * * * * * * * * * * * * * * * * * * */


    $('#solutions').find('.card').each(function(){
        setCardShadeHeight($(this));
    });



    $(window).on('resize', function(){
        $('#solutions').find('.card').each(function(){
            setCardShadeHeight($(this));
        });
    });


    function setCardShadeHeight(card){
        var cardHeight = card.outerHeight();
        card.find('.card-shade').css({
            height: cardHeight + 'px'
        })
    }


    /* * * * * * * * * * * * * * * * * * * * *
        CONTROL TEL LINKS
     * * * * * * * * * * * * * * * * * * * * */

    controlTelLinks();

    $(window).on('resize', function(){
        controlTelLinks();
    });

    function controlTelLinks() {
        if ( !isMobile() || Device_Type() == 'Desktop' ) {
            $('.js-tel').each(function(){
                $(this).addClass('disabled-link');
                $(this).on('click', function(e){
                    e.preventDefault();
                });
            })
        } else {
            $(this).removeClass('disabled-link');
        }
    }

});


function isMobile() {
    if ($(window).width() < 992) {
        return true;
    }
    else {
        return false;
    }
}


function Device_Type() {
    var Return_Device;
    if(/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile|w3c|acs\-|alav|alca|amoi|audi|avan|benq|bird|blac|blaz|brew|cell|cldc|cmd\-|dang|doco|eric|hipt|inno|ipaq|java|jigs|kddi|keji|leno|lg\-c|lg\-d|lg\-g|lge\-|maui|maxo|midp|mits|mmef|mobi|mot\-|moto|mwbp|nec\-|newt|noki|palm|pana|pant|phil|play|port|prox|qwap|sage|sams|sany|sch\-|sec\-|send|seri|sgh\-|shar|sie\-|siem|smal|smar|sony|sph\-|symb|t\-mo|teli|tim\-|tosh|tsm\-|upg1|upsi|vk\-v|voda|wap\-|wapa|wapi|wapp|wapr|webc|winw|winw|xda|xda\-) /i.test(navigator.userAgent))
    {
        if(/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i.test(navigator.userAgent))
        {
            Return_Device = 'Tablet';
        }
        else
        {
            Return_Device = 'Mobile';
        }
    }
    else if(/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i.test(navigator.userAgent))
    {
        Return_Device = 'Tablet';
    }
    else
    {
        Return_Device = 'Desktop';
    }

    return Return_Device;
}
