<?php
$languageCode  = ipContent()->getCurrentLanguage()->getCode();
$aboutUs = ipDb()->fetchAll( 'SELECT * FROM ip_aboutus au LEFT JOIN ip_aboutus_language aul ON au.id = aul.itemId
WHERE aul.language = "' . $languageCode . '"
ORDER BY au.sort_order ASC
' )
?>

<section class="page-section section-framed" id="about">
	<div class="container-fluid main-wrapper section-imaged-wrapper">
		<div class="row">
			<div class="col-sm-12">
				<?= ipSlot( 'text', array(
					'id'      => 'aboutUs-header',
					'tag'     => 'h2',
					'class'   => 'header-2 text-center',
					'default' => 'Apie mus'
				) ) ?>
			</div>
		</div>
		<div class="row same-height-grid">
			<?php $i = 1;
			foreach ( $aboutUs as $about_us ) { ?>
				<?php
				$options   = array(
					'type'    => 'center',
					'width'   => 655,
					'height'  => 480,
					'quality' => 100,
				);
				$thumbnail = ipReflection( $about_us['image'], $options );
				?>
				<div class="row-height row-sm-height">
					<?php if ( $i % 2 != 0 ) { ?>
						<div class="card grid-card col-xs-12 col-sm-6 col-sm-height col-xs-middle bg-gray">
							<div class="card-content">
								<h2 class=card-title"><?= $about_us['title'] ?></h2>
								<div class="card-tb">
									<?= $about_us['text'] ?>
								</div>
							</div>
						</div>
						<div class="card grid-card col-xs-12 col-sm-6 col-sm-height col-xs-middle bg-img"
						     style="background-image: url(<?= ipFileUrl( $thumbnail ) ?>)">
						</div>
					<?php } else { ?>
						<div class="card grid-card col-xs-12 col-sm-6 col-sm-height col-xs-middle bg-img"
						     style="background-image: url(<?= ipFileUrl( $thumbnail ) ?>)">
						</div>
						<div class="card grid-card col-xs-12 col-sm-6 col-sm-height col-xs-middle bg-gray">
							<div class="card-content">
								<h2 class=card-title"><?= $about_us['title'] ?></h2>
								<div class="card-tb">
									<?= $about_us['text'] ?>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<?php $i ++;
			} ?>
		</div>
	</div>
</section>