<?php $languageCode  = ipContent()->getCurrentLanguage()->getCode(); ?>
<form class="form-horizontal form-underlined-inputs contactForm">
    <input type="hidden" name="lang" value="<?= $languageCode ?>">
    <div class="form-group">
        <div class="form-group-input col-sm-12 col-md-4">
            <input name="name" type="text" class="form-control" placeholder="<?= __( 'Vardas ir pavardė', 'Avedus' ); ?>">
        </div>
        <div class="form-group-input col-sm-12 col-md-4">
            <input name="email" type="email" class="form-control" placeholder="<?= __( 'El. pašto adresas', 'Avedus' ); ?>">
        </div>
        <div class="form-group-input col-sm-12 col-md-4 ">
            <input name="topic" type="text" class="form-control" placeholder="<?= __( 'Tema', 'Avedus' ); ?>">
        </div>
    </div>
    <div class="form-group">
        <div class="form-group-input col-sm-12">
            <textarea name="message" class="form-control" placeholder="<?= __( 'Žinutė', 'Avedus' ); ?>" rows="6"></textarea>
        </div>
    </div>
    <div class="form-group form-footer">
        <div class="col-sm-12 text-center">
            <div id="alert-success" class="alert-success"></div>
            <input type="hidden" name="mainMail" value="<?= ipStorage()->get( 'AppControl', 'email_' . $languageCode ) ?>">
            <button type="submit" class="btn btn-main"><?= __('SIŲSTI ŽINUTĘ', 'Avedus') ?></button>
        </div>
    </div>
</form>