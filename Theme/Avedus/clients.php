<?php
$languageCode = ipContent()->getCurrentLanguage()->getCode();
$clients      = ipDb()->fetchAll( 'SELECT * FROM ip_category cat LEFT JOIN ip_category_language catl ON cat.id = catl.itemId
WHERE catl.language = "' . $languageCode . '"
ORDER BY cat.sort_order ASC
' )
?>

<div class="col-sm-12">


    <div class="custom-tabs" id="#clients-tabs">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
			<?php $i = 0;
			foreach ( $clients as $client ) { ?>
                <li role="presentation" <?= $i == 0 ? 'class="active"' : '' ?>>
                    <a class="capabilities-link" href="#<?= str_replace(')', '', str_replace('(', '',str_replace(',', '',str_replace( ' ', '_', htmlspecialchars($client['category_title'] ))))); ?>"
                       role="tab" data-toggle="tab">
						<?= htmlspecialchars($client['category_title']) ?>
                    </a>
                </li>
				<?php $i ++;
			} ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
			<?php $i = 0;
			foreach ( $clients as $client ) { ?>

                <div role="tabpanel" class="tab-pane <?= $i == 0 ? 'active' : '' ?>"
                     id="<?= str_replace(')', '', str_replace('(', '',str_replace(',', '',str_replace( ' ', '_', htmlspecialchars($client['category_title'] ))))); ?>">
                    <div class="row">
                        <ul class="list-inline logos-list">
							<?php $clientInfo = ipDb()->fetchAll( 'SELECT * FROM ip_category_clients catcl WHERE catcl.category_id =' . $client['id'] ) ?>
							<?php foreach ( $clientInfo as $client_info ) { ?>
                                <?php
                                $options   = array(
                                    'type'    => 'center',
                                    'width'   => 360,
                                    'height'  => 150,
                                    'quality' => 100,
                                );
                                $thumbnail = ipReflection($client_info['logo'] , $options );
                                ?>

                                <li>
                                    <a title="<?= htmlspecialchars($client_info['client_title'])?>" class="gray-filter-wrapper"
                                       href="<?= $client_info['client_url'] != null ? $client_info['client_url'] : '#' ?>"
                                       target="_blank">
                                        <img class="img-responsive"
                                             src="<?= ipFileUrl( $thumbnail ) ?>"
                                             alt="<?= htmlspecialchars($client_info['client_title'])?>" title="<?= htmlspecialchars($client_info['client_title'])?>">
                                    </a>
                                </li>
							<?php } ?>

                        </ul>
                    </div>
                </div>
				<?php $i ++;
			} ?>


        </div>

    </div>
</div>