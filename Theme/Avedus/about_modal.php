<?php
$languageCode = ipContent()->getCurrentLanguage()->getCode();
$about      = ipDb()->fetchAll( 'SELECT * FROM ip_aboutModal am LEFT JOIN ip_aboutModal_language aml ON am.id = aml.itemId
WHERE aml.language = "' . $languageCode . '"' )
?>

<?php foreach ($about as $aboutModal){?>
<div class="modal fade pagelike-modal" id="aboutModal" tabindex="-1" role="dialog"
     aria-labelledby="aboutModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content text-dark">

			<div class="modal-content-wrapper">
				<div class="modal-header">

                    <?php
                    $options   = array(
                        'type'    => 'center',
                        'width'   => 970,
                        'height'  => 260,
                        'quality' => 100,
                    );
                    $thumbnail = ipReflection($aboutModal['img'] , $options );
                    ?>

                    <div class="header-imaged" style="background-image: url(<?= ipFileUrl($thumbnail)?>)">
                        <div class="header-overlay"></div>
                        <div class="header-content">
                            <h2 class="header header-2 text-center">
                                <?= $aboutModal['title']?>
                            </h2>
                        </div>
                    </div>
                </div>

				<div class="modal-body">
					<section>
						<div class="container-fluid">
							<div class="row">
								<div class="col-sm-12 tb tb-spacedout text-lighter">
									<?= $aboutModal['text']?>
								</div>
							</div>
						</div>
					</section>

					<section>
						<div class="container-fluid">
							<div class="row mt-80">
								<div class="col-sm-12">
									<h2 class="header-3 text-center">
										<?= $aboutModal['policy_title']?>
									</h2>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 tb tb-spacedout text-lighter mt-40 list-ticked">
									<?= $aboutModal['policy_text']?>
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="container-fluid">
							<div class="row mt-40">
								<div class="col-sm-12">
									<h2 class="header-3 text-center">
										<?= __('Susisiekti', 'Avedus') ?>
									</h2>
								</div>
							</div>
							<div class="row mt-40">
								<div id="formAbout" class="col-sm-12">
									<?= ipView('form.php')->render() ?>
								</div>
							</div>
						</div>
					</section>
				</div>

			</div>
		</div>
	</div>
    <div class="modal-footer text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">
                                 <i class="fa fa-times"></i>
                                <p><?= __('Grįžti atgal', 'Avedus') ?></p>
                             </span>
        </button>
    </div>
</div>
<?php } ?>