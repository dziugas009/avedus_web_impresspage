<?php
$languageCode = ipContent()->getCurrentLanguage()->getCode();
$sliders      = ipDb()->fetchAll( 'SELECT * FROM ip_slider
ORDER BY sort_order ASC
' )
?>
<section class="page-section section-framed" id="main-slider">

    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-12">

				<?= ipSlot( 'text', array(
					'id'      => 'gallery-header1',
					'tag'     => 'h2',
					'class'   => 'header-2 text-center',
					'default' => 'Galerija'
				) ) ?>
            </div>
        </div>

        <div class="row gallery-row">

			<?php $i = 0;
			foreach ( $sliders as $slider ) { ?>
                <?php
				$options1   = array(
					'type'    => 'center',
					'width'   => 1200,
					'height'  => 800,
					'quality' => 100,
				);
				$thumbnail1 = ipReflection( $slider['image'], $options1 );

                $options1mob   = array(
                    'type'    => 'center',
                    'width'   => 700,
                    'height'  => 400,
                    'quality' => 100,
                );
                $thumbnailmobile = ipReflection( $slider['image'], $options1mob );

				$options2   = array(
					'type'    => 'center',
					'width'   => 413,
					'height'  => 250,
					'quality' => 100,
				);
				$thumbnail2 = ipReflection( $slider['image'], $options2 );

                ?>
                <div class="col-sm-6 col-md-4<?= $i > 5 ? ' moreImg' : '' ?>">
                    <?php if (!ipIsManagementState()) { ?>
                    <a class="gallery-img-link" data-lightbox="gallery"
                       href="<?= ipFileUrl($thumbnailmobile) ?>" data-img-url="<?= ipFileUrl($thumbnail1) ?>" data-mobile-img-url="<?= ipFileUrl($thumbnailmobile) ?>">
                        <?php } ?>

                        <div class="gallery-img" style="background-image: url('<?= ipFileUrl( $thumbnail2 ) ?>')"></div>
                        <?php if (!ipIsManagementState()) { ?>
                    </a> <?php } ?>
                </div>
				<?php $i ++;
			} ?>
        </div>
		<?php if ( $i > 6 ) { ?>

            <div class="col-sm-12 text-center">
                <button type="submit" id="moreGallery" class="btn btn-main"><?= __( 'RODYTI DAUGIAU', 'Avedus' ); ?></button>
            </div>
		<?php } ?>
    </div>
</section>
