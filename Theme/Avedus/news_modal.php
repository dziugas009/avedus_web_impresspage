<?php
$languageCode = ipContent()->getCurrentLanguage()->getCode();
$news_slider  = ipDb()->fetchAll( 'SELECT * FROM ip_news nw LEFT JOIN ip_news_language nwl ON nw.id = nwl.itemId
WHERE nwl.language = "' . $languageCode . '" AND is_visible=0
ORDER BY nw.date DESC
' );
?>
<div class="modal fade pagelike-modal" id="newsModal" tabindex="-1" role="dialog" aria-labelledby="newsModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content text-dark">

            <div id="modalDateHeader" class="modal-content-wrapper">
                <div class="modal-header">
                    <h2 id="newsModalTitle" class="header-2 text-center modal-title modal-data">
                        Quisque porttitor non nunc vel bibendum
                    </h2>
                    <p class="text-main text-center modal-date">
                        <b id="newsModalReleaseDate" data-mask='0000 / 00 / 00'>20170828</b>
                    </p>
                </div>

                <div class="modal-body">
                    <section class="inner-wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <img id="newsModalImg" class="news-img img-responsive modal-image"
                                         alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 tb tb-spacedout text-lighter modal-data modal-body-text list-ticked"
                                     id="newsModalText">
                                    <p>
                                        Lietuvoje kasdien bent viena įmonė patiria žalą dėl internetinių grėsmių.
                                        Kibernetinės atakos gali sukelti materialinius nuostolius arba nepataisomai
                                        sugadinti įmonės reputaciją. Tik pilnavertė įeinančio interneto srauto
                                        apsauga padeda apsaugoti tinklą nuo virusų, nepageidaujamų el. laiškų (ang.
                                        spam), įsilaužimų, atakų ar duomenų vagysčių.
                                    </p>
                                    <p>
                                        AVEDUS tinklo saugumo ekspertai vertina įmonės tinklo būklę, randa
                                        pažeidžiamumus ir padeda apsisaugoti nuo žalos.
                                    </p>
                                    <p>
                                        Negaiškite laiko ir nešvaistykite resursų. Susitarkime dėl susitikimo, kurio
                                        metu gausite esamos situacijos analizę bei galimus sprendimus.
                                    </p>
                                    <p>
                                        Kodėl mes? Todėl, kad mes patikimi, greiti, lankstūs ir orentuoti į klientą.
                                        Taip apie mus atsiliepia mūsų klientai.
                                    </p>
                                    <p>
                                        Specializuojamės: Ugniasienių (ang. firewall) implemantavimas į tinklą
                                        el.pašto apsauga (ang. email server safeguard), belaidžio tinklo sukūrimas
                                        ir valdymas (ang. WiFi , prevencija nuo svarbių duomenų praradimo (DLP),
                                        aplikacijų kontrolė (ang. App control), srauto valdymas (ang. Network
                                        traffic management), vartotojų autentifikavimas ir valdymas (ang. user
                                        autentification and managment), internetinių svetainių apsauga (ang. website
                                        vulnerability scan), apsauga nuo DDos atakų, mobilių duomenų apsauga (ang.
                                        Mobile data prevention), jungimosi prie tinklo kontrolė (ang. SSL VPN access
                                        control), duomenų srauto tarp serverių optimizavimas (ang. Server Load
                                        balancing) ir kiti sprendimai.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section>
                        <div class="container-fluid">
                            <div class="row text-center mt-40">
                                <div class="col-sm-12">
                                    <h2 class="header-2">
										<?= __( 'Kitos naujienos', 'Avedus' ) ?>
                                    </h2>
                                </div>
                            </div>

                            <div class="row mt-40">
                                <div class="swiper-container swiper-fadedout" id="swiper-news-modal">
                                    <div class="swiper-wrapper">
										<?php $i = 0;
										foreach ( $news_slider as $news ) { ?>
											<?php if ( $news['header'] != null && $news['card_first_text'] != null && $news['card_rest_text'] != null ) {
												?>
                                                <div class="swiper-slide">

                                                    <?php

                                                    $mioptions   = array(
                                                        'type'    => 'center',
                                                        'width'   => 970,
                                                        'height'  => 600,
                                                        'quality' => 100,
                                                    );
                                                    $mainImage = ipReflection( $news['image'], $mioptions );

                                                    $mimoboptions   = array(
                                                        'type'    => 'center',
                                                        'width'   => 700,
                                                        'height'  => 400,
                                                        'quality' => 100,
                                                    );
                                                    $mainImageMob = ipReflection( $news['image'], $mimoboptions );
                                                    ?>


                                                    <a href="#" class="specialClass card card-news"
                                                       data-news-image="<?= ipFileUrl( $mainImage ) ?>"
                                                       data-news-image-mobile="<?= ipFileUrl( $mainImageMob ) ?>"
                                                       data-news-text="<?=  htmlspecialchars( $news['card_first_text'], ENT_QUOTES ) ?><?= htmlspecialchars( $news['card_rest_text'], ENT_QUOTES) ?>">

                                                        <label class="text-main js-mask-date">
															<?php
															$date = $news['date'];
															$dt   = new DateTime( $date );
															echo $dt->format( 'Ymd' )
															?>
                                                        </label>
                                                        <input type="hidden" id="titleNews"
                                                               value="<?= htmlspecialchars( $news['header'], ENT_QUOTES  ) ?>">
                                                        <input type="hidden" id="firstNews"
                                                               value="<?= htmlspecialchars( $news['card_first_text'], ENT_QUOTES ) ?>">
                                                        <input type="hidden" id="restNews"
                                                               value="<?= htmlspecialchars( $news['card_rest_text'], ENT_QUOTES ) ?>">
                                                        <input type="hidden" class="dateNews" value="<?= $dt->format( 'Y / m / d' ) ?>">

                                                        <input type="hidden" id="imgNews"
                                                               value="<?= ipFileUrl( $mainImage ) ?>">

                                                        <input type="hidden" id="imgNewsMob"
                                                               value="<?= ipFileUrl( $mainImageMob ) ?>">
                                                        <?php
                                                        $options   = array(
                                                            'type'    => 'center',
                                                            'width'   => 298,
                                                            'height'  => 140,
                                                            'quality' => 100,
                                                        );
                                                        $thumbnail = ipReflection( $news['image'] , $options );
                                                        ?>

                                                        <div class="card-img-holder bg-img"
                                                             style="background-image: url(<?= ipFileUrl($thumbnail) ?>)">
                                                        </div>

                                                        <div class="card-text">
                                                            <h3 class="card-title">
																<?= $news['header'] ?>
                                                            </h3>

                                                            <div class="tb truncate">
																<p><?= $news['card_first_text'] ?></p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
												<?php $i ++;
											}
										} ?>

                                    </div>
                                    <div class="swiper-pagination swiper-news-pagination-modal"></div>


                                    <div class="swiper-news-arrow-modal swiper-button-prev"></div>
                                    <div class="swiper-news-arrow-modal swiper-button-next"></div>
                                </div>
                            </div>

                        </div>
                    </section>
                </div>

            </div>
        </div>
    </div>

    <div class="modal-footer text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <i class="fa fa-times"></i>
                <p><?= __( 'Grįžti atgal', 'Avedus' ) ?></p>
            </span>
        </button>
    </div>
</div>