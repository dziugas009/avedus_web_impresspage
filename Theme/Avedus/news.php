<?php
$languageCode = ipContent()->getCurrentLanguage()->getCode();
$news_slider  = ipDb()->fetchAll( 'SELECT * FROM ip_news nw LEFT JOIN ip_news_language nwl ON nw.id = nwl.itemId
WHERE nwl.language = "' . $languageCode . '" AND is_visible=0
ORDER BY nw.date DESC
' )
?>

<section class="page-section section-framed" id="news">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
				<?= ipSlot( 'text', array(
					'id'      => 'news-header1',
					'tag'     => 'h2',
					'class'   => 'header-2 text-center',
					'default' => 'Naujienos'
				) ) ?>

            </div>
        </div>

        <div class="row">
            <div class="swiper-container swiper-fadedout" id="swiper-news">
                <div class="swiper-wrapper">
					<?php $i = 0;
					foreach ( $news_slider as $news ) {
						if ( $news['header'] != null && $news['card_first_text'] != null && $news['card_rest_text'] != null ) {
							?>
							<?php
							$options   = array(
								'type'    => 'center',
								'width'   => 298,
								'height'  => 140,
								'quality' => 100,
							);
							$thumbnail = ipReflection( $news['image'], $options );

                            $mioptions   = array(
                                'type'    => 'center',
                                'width'   => 970,
                                'height'  => 600,
                                'quality' => 100,
                            );
                            $mainImage = ipReflection( $news['image'], $mioptions );

                            $mimoboptions   = array(
                                'type'    => 'center',
                                'width'   => 700,
                                'height'  => 400,
                                'quality' => 100,
                            );
                            $mainImageMob = ipReflection( $news['image'], $mimoboptions );
                            ?>

                            <div class="swiper-slide">
                                <a href="#" class="specialClass card card-news" data-toggle="modal"
                                   data-target="#newsModal">

                                    <label class="text-main js-mask-date">
										<?php
										$date = $news['date'];
										$dt   = new DateTime( $date );
										echo $dt->format( 'Y / m / d' )
										?>
                                    </label>
                                    <div class="card-img-holder bg-img"
                                         style="background-image: url(<?= ipFileUrl( $thumbnail ) ?>)">
                                    </div>
                                    <div class="card-text">
                                        <h3 class="card-title">
											<?= $news['header']  ?>
                                        </h3>
                                        <input type="hidden" id="titleNews"
                                               value="<?= htmlspecialchars( $news['header'], ENT_QUOTES  ) ?>">
                                        <input type="hidden" id="firstNews"
                                               value="<?= htmlspecialchars( $news['card_first_text'], ENT_QUOTES ) ?>">
                                        <input type="hidden" id="restNews"
                                               value="<?= htmlspecialchars( $news['card_rest_text'], ENT_QUOTES ) ?>">
                                        <input type="hidden" class="dateNews" value="<?= $dt->format( 'Y / m / d' ) ?>">

                                        <input type="hidden" id="imgNews"
                                               value="<?= ipFileUrl( $mainImage ) ?>">

                                        <input type="hidden" id="imgNewsMob"
                                               value="<?= ipFileUrl( $mainImageMob ) ?>">

                                        <div class="truncate tb">
											<p><?= $news['card_first_text'] ?></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
							<?php $i ++;
						}
					} ?>
                </div>
                <input type="hidden" id="newsCounter" value="<?= $i ?>">
                <div class="swiper-pagination swiper-news-pagination"></div>

                <div class="swiper-news-arrow swiper-button-prev"></div>
                <div class="swiper-news-arrow swiper-button-next"></div>
            </div>


        </div>
    </div>
</section>
