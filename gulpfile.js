var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');

var config = {
    "developmentPath": "Theme/Avedus/assets/",
    "env": "development"
};

var jsDest = 'Theme/Avedus/assets/js';

gulp.task('styles', function() {
    return gulp.src(config.developmentPath + 'scss')
        .pipe(plumber())
        .pipe(concat('style.scss'))
        .pipe(gulp.dest(config.developmentPath + 'scss'))
});

gulp.task('sass', function(){
    gulp.src(config.developmentPath + 'scss/style.scss')
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(config.developmentPath + 'css'))
        .pipe(rename('style.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(config.developmentPath + 'css'))

});

gulp.task('js', function() {
    return gulp.src('Theme/Avedus/assets/js/script.js')
        .pipe(plumber())
        .pipe(rename('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.developmentPath + 'js'));
});

gulp.task('minify-css', function() {
    return gulp.src([
        'Theme/Avedus/assets/css/bootstrap.min.css',
        'Theme/Avedus/assets/css/animate.css',
        'Theme/Avedus/assets/css/font-awesome.min.css',
        'Theme/Avedus/assets/css/swiper.min.css',
        'Theme/Avedus/assets/css/lightbox.css',
        ])
        .pipe(concat('libs.css'))
        .pipe(gulp.dest(config.developmentPath + 'css'))
        .pipe(rename('libs.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(config.developmentPath + 'css'));
});

gulp.task('minify-js', function() {
    return gulp.src([
        'Theme/Avedus/assets/js/libs/swiper.jquery.min.js',
        'Theme/Avedus/assets/js/libs/jquery.mask.min.js',
        'Theme/Avedus/assets/js/libs/jquery.dotdotdot.js',
        'Theme/Avedus/assets/js/libs/lightbox.js'
    ])
        .pipe(concat('libs.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});


gulp.task('default', ['styles', 'sass', 'js'], function (){
    gulp.watch(config.developmentPath + 'scss/*.scss', ['sass']);
    gulp.watch(config.developmentPath + 'scss/*/*.scss', ['sass']);
    gulp.watch(config.developmentPath + 'js/script.js', ['js']);
});


gulp.task('minify', ['minify-js', 'minify-css']);
